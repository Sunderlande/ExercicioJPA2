package domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_funcionario", uniqueConstraints = {
		@UniqueConstraint(name = "uk_rg", columnNames = { "rg", "rg_orgao_expedidor", "rg_uf" }) })
public class Funcionario {

	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cpf;

	@Column(name = "nome_funcionario", length = 40, nullable = false, insertable = false, updatable = false)
	private String nome;

	@Column(name = "rg_funcionario", columnDefinition = "char(12)", nullable = true)
	private String rg;

	@Column(name = "rg_orgao_expedidor", length = 20, nullable = true)
	private String rgOrgaoExpedidor;

	@Column(name = "rg_uf", columnDefinition = "char(2)", nullable = true)
	private String rgUf;

	@ElementCollection
	@CollectionTable(name = "tab_funcionarios_telefone", joinColumns = @JoinColumn(name = "cpf_funcionario", columnDefinition = "char(12)", foreignKey = @ForeignKey(name = "fk_telefone_funcionario")))
	private List<String> telefones;

	@Column(name = "data_nascimento")
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;

	//@Embedded
	@Column(nullable = false)
	private Endereco endereco;

	public Funcionario() {
		super();
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getRgOrgaoExpedidor() {
		return rgOrgaoExpedidor;
	}

	public void setRgOrgaoExpedidor(String rgOrgaoExpedidor) {
		this.rgOrgaoExpedidor = rgOrgaoExpedidor;
	}

	public String getRgUf() {
		return rgUf;
	}

	public void setRgUf(String rgUf) {
		this.rgUf = rgUf;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
		result = prime * result + ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((rg == null) ? 0 : rg.hashCode());
		result = prime * result + ((rgOrgaoExpedidor == null) ? 0 : rgOrgaoExpedidor.hashCode());
		result = prime * result + ((rgUf == null) ? 0 : rgUf.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (dataNascimento == null) {
			if (other.dataNascimento != null)
				return false;
		} else if (!dataNascimento.equals(other.dataNascimento))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (rg == null) {
			if (other.rg != null)
				return false;
		} else if (!rg.equals(other.rg))
			return false;
		if (rgOrgaoExpedidor == null) {
			if (other.rgOrgaoExpedidor != null)
				return false;
		} else if (!rgOrgaoExpedidor.equals(other.rgOrgaoExpedidor))
			return false;
		if (rgUf == null) {
			if (other.rgUf != null)
				return false;
		} else if (!rgUf.equals(other.rgUf))
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Funcionario [cpf=" + cpf + ", nome=" + nome + ", rg=" + rg + ", rgOrgaoExpedidor=" + rgOrgaoExpedidor
				+ ", rgUf=" + rgUf + ", telefones=" + telefones + ", dataNascimento=" + dataNascimento + ", endereco="
				+ endereco + "]";
	}

}
