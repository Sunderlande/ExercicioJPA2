package domain;

public enum SituacaoVendedorEnum {

	ATIVO("ATV"), SUSPENSO("SPN");

	public String ValorSigla;

	SituacaoVendedorEnum(String sigla) {
		this.ValorSigla = sigla;
	}

	public String getValorSigla() {
		return ValorSigla;
	}
}
