package converters;

import domain.SituacaoVendedorEnum;

public class SituacaoVendedorConverter {

	public String convertToDatabaseColumn(SituacaoVendedorEnum attribute) {
		if (attribute != null) {
			return attribute.getValorSigla();
		}
		return null;
	}


	public SituacaoVendedorEnum convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return SituacaoVendedorEnum.valueOf(dbData);
	}
	
}
