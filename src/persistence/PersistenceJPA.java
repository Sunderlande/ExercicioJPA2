package persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceJPA {

	public static void main(String[] args) {
		
		EntityManagerFactory emf = null;
		
		try {
			emf = Persistence.createEntityManagerFactory("ExercicioJPA2");
			EntityManager em = emf.createEntityManager();
			
			em.getTransaction().begin();
			
			em.getTransaction().commit();
			
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}
}
