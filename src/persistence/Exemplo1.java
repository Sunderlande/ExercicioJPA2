package persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import domain.Administrativo;
import domain.Cidade;
import domain.Estado;
import domain.PessoaJuridica;
import domain.RamoAtividade;
import domain.Vendedor;

public class Exemplo1 {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("pu1");
			EntityManager em = emf.createEntityManager();

			// insert de estados
			em.getTransaction().begin();
			Estado estado1 = new Estado();
			estado1.setNome("Bahia");
			estado1.setSigla("BA");
			em.getTransaction().commit();

			em.getTransaction().begin();
			Estado estado2 = new Estado();
			estado1.setNome("Pernambuco");
			estado1.setSigla("PE");
			em.getTransaction().commit();

			em.getTransaction().begin();
			Estado estado3 = new Estado();
			estado1.setNome("Acre");
			estado1.setSigla("AC");
			em.getTransaction().commit();

			// insert de cidades
			em.getTransaction().begin();
			Cidade cidade1 = new Cidade();
			cidade1.setEstado(estado1);
			cidade1.setNome("Salvador");
			cidade1.setSigla("BA");
			em.getTransaction().commit();

			em.getTransaction().begin();
			Cidade cidade2 = new Cidade();
			cidade2.setEstado(estado1);
			cidade2.setNome("Sim�es Filho");
			cidade2.setSigla("BA");
			em.getTransaction().commit();

			em.getTransaction().begin();
			Cidade cidade3 = new Cidade();
			cidade3.setEstado(estado1);
			cidade3.setNome("Sim�es Filho");
			cidade3.setSigla("BA");
			em.getTransaction().commit();

			em.getTransaction().begin();
			Cidade cidade4 = new Cidade();
			cidade4.setEstado(estado2);
			cidade4.setNome("Recife");
			cidade4.setSigla("PE");
			em.getTransaction().commit();

			em.getTransaction().begin();
			Cidade cidade5 = new Cidade();
			cidade5.setEstado(estado2);
			cidade5.setNome("Jaboat�o de Guararapes");
			cidade5.setSigla("PE");
			em.getTransaction().commit();

			// insert de ramo de atividade
			em.getTransaction().begin();
			RamoAtividade ramo_atividade1 = new RamoAtividade();
			ramo_atividade1.setId(1);
			ramo_atividade1.setNome("Venda");
			em.getTransaction().commit();

			em.getTransaction().begin();
			RamoAtividade ramo_atividade2 = new RamoAtividade();
			ramo_atividade2.setId(2);
			ramo_atividade2.setNome("Venda");
			em.getTransaction().commit();

			em.getTransaction().begin();
			RamoAtividade ramo_atividade3 = new RamoAtividade();
			ramo_atividade3.setId(3);
			ramo_atividade3.setNome("Venda");
			em.getTransaction().commit();
			
			
			// insert de pessoa juridica
			em.getTransaction().begin();
			PessoaJuridica pessoa_juridica1 = new PessoaJuridica();
			pessoa_juridica1.setCnpj("1111");
			pessoa_juridica1.setFaturamento(1000.0);
			pessoa_juridica1.setNome("Jo�o");
			pessoa_juridica1.setRamosAtividade((List<RamoAtividade>) ramo_atividade1);
			pessoa_juridica1.setVendedores(vendedores);
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			PessoaJuridica pessoa_juridica2 = new PessoaJuridica();
			pessoa_juridica2.setCnpj("2222");
			pessoa_juridica2.setFaturamento(2000.0);
			pessoa_juridica2.setNome("Pedro");
			pessoa_juridica2.setRamosAtividade((List<RamoAtividade>) ramo_atividade1);
			pessoa_juridica2.setVendedores(vendedores);
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			PessoaJuridica pessoa_juridica3 = new PessoaJuridica();
			pessoa_juridica3.setCnpj("3333");
			pessoa_juridica3.setFaturamento(3000.0);
			pessoa_juridica3.setNome("Francisco");
			pessoa_juridica3.setRamosAtividade((List<RamoAtividade>) ramo_atividade1);
			pessoa_juridica3.setVendedores(vendedores);
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			PessoaJuridica pessoa_juridica4 = new PessoaJuridica();
			pessoa_juridica4.setCnpj("3333");
			pessoa_juridica4.setFaturamento(3000.0);
			pessoa_juridica4.setNome("Francisco");
			pessoa_juridica4.setRamosAtividade((List<RamoAtividade>) ramo_atividade2);
			pessoa_juridica4.setVendedores(vendedores);
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			PessoaJuridica pessoa_juridica5 = new PessoaJuridica();
			pessoa_juridica5.setCnpj("3333");
			pessoa_juridica5.setFaturamento(3000.0);
			pessoa_juridica5.setNome("Francisco");
			pessoa_juridica5.setRamosAtividade((List<RamoAtividade>) ramo_atividade2);
			pessoa_juridica5.setVendedores(vendedores);
			em.getTransaction().commit();
			
			
			// inserindo vendedores
			em.getTransaction().begin();
			Vendedor vendedor1 = new Vendedor();
			vendedor1.setClientes(clientes);
			vendedor1.setCpf("11111");
			vendedor1.setDataNascimento('1988-12-12');
			vendedor1.setEndereco("Rua A");
			vendedor1.setNome("fulanoa");
			vendedor1.setPercentualComissao(10.0);
			vendedor1.setRg("1111");
			vendedor1.setRgUf("BA");
			em.getTransaction().commit();
			
			
			em.getTransaction().begin();
			Vendedor vendedor2 = new Vendedor();
			vendedor2.setClientes(clientes);
			vendedor2.setCpf("22222");
			vendedor2.setDataNascimento('1983-04-04');
			vendedor2.setEndereco("Rua B");
			vendedor2.setNome("fulanoB");
			vendedor2.setPercentualComissao(10.0);
			vendedor2.setRg("2222");
			vendedor2.setRgUf("BA");
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			Vendedor vendedor3 = new Vendedor();
			vendedor3.setClientes(clientes);
			vendedor3.setCpf("33333");
			vendedor3.setDataNascimento('1983-04-04');
			vendedor3.setEndereco("Rua C");
			vendedor3.setNome("fulanoC");
			vendedor3.setPercentualComissao(10.0);
			vendedor3.setRg("3333");
			vendedor3.setRgUf("BA");
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			Vendedor vendedor4 = new Vendedor();
			vendedor4.setClientes(clientes);
			vendedor4.setCpf("44444");
			vendedor4.setDataNascimento('1983-04-04');
			vendedor4.setEndereco("Rua D");
			vendedor1.setNome("fulanoD");
			vendedor1.setPercentualComissao(10.0);
			vendedor1.setRg("4444");
			vendedor1.setRgUf("BA");
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			Vendedor vendedor5 = new Vendedor();
			vendedor5.setClientes(clientes);
			vendedor5.setCpf("55555");
			vendedor5.setDataNascimento('1983-04-04');
			vendedor5.setEndereco("Rua E");
			vendedor1.setNome("fulanoE");
			vendedor1.setPercentualComissao(10.0);
			vendedor1.setRg("55555");
			vendedor1.setRgUf("BA");
			em.getTransaction().commit();
			
			//inserir funcionario administrativo
			em.getTransaction().begin();
			Administrativo administrativo1 = new Administrativo();
			administrativo1.setCpf("12345");
			administrativo1.setDataNascimento("1999-07-21");
			administrativo1.setEndereco("Rua qualquer2");
			administrativo1.setNome("Administrativo 1");
			administrativo1.setRg("1234354965");
			administrativo1.setRgUf("BA");
			administrativo1.setTelefones(telefones);
			em.getTransaction().commit();
			
			
			em.getTransaction().begin();
			Administrativo administrativo2 = new Administrativo();
			administrativo2.setCpf("12325");
			administrativo2.setDataNascimento("1999-08-23");
			administrativo2.setEndereco("Rua qualquer2");
			administrativo2.setNome("Administrativo 2");
			administrativo2.setRg("123435496eded5");
			administrativo2.setRgUf("BA");
			administrativo2.setTelefones(telefones);
			em.getTransaction().commit();
			
			em.getTransaction().begin();
			Administrativo administrativo3 = new Administrativo();
			administrativo3.setCpf("12311");
			administrativo3.setDataNascimento("1929-07-23");
			administrativo3.setEndereco("Rua qualquer");
			administrativo3.setNome("Administrativo 3");
			administrativo3.setRg("12343232323");
			administrativo3.setRgUf("BA");
			administrativo3.setTelefones(telefones);
			em.getTransaction().commit();
			
			
			
			
		} finally {
			// TODO: handle finally clause
		}
	}

}
