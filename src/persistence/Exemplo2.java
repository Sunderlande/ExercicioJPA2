package persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import domain.PessoaJuridica;
import domain.Vendedor;

/*
 * a) Clientes por vendedor
Filtro: nome vendedor
Resultado: nome e endere�o dos clientes desse vendedor -- select nome_vendedor


b) Ramos de atividade
Filtro: nome do ramo atividade
Resultado: nome, telefone e endere�o dos vendedores que atendem � clientes desse
ramo de atividade


c) Funcionarios ativos
Filtro: (nenhum)
Resultado: cpf e nome dos funcion�rios que t�m situa��o ATIVO


d) Estados sem cidade
Filtro: (nenhum)
Resultado: sigla e nome dos estados que n�o possuem cidades cadastradas


e) Quantidade vendedores por cidade
Filtro: (nenhum)
Resultado: nome da cidade e quantidade de vendedores dessa cidade


f) Vendedores de uma cidade
Filtro: nome da cidade
Resultado: nome e telefone do vendedor
 */

public class Exemplo2 {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("pu1");
		EntityManager em = emf.createEntityManager();
		try {

			em.getTransaction().begin();
			TypedQuery<Vendedor> queryVendedor = em
					.createQuery("select vd from Vendedor vd where vd.nomeFuncionario=:nome", Vendedor.class);
			queryVendedor.setParameter("nome", "Ana");

			List<Vendedor> vendedores = queryVendedor.getResultList();
			if (vendedores.isEmpty() == false) {
				for (Vendedor vd : vendedores) {
					System.out.println("Nome do Vendedor: " + vd.getNome());
					if (vd.getClientes().isEmpty() == false) {
						for (PessoaJuridica pj : vd.getClientes()) {
							System.out.println(" | " + pj.getNome());
						}
					} else {
						System.out.println(" | No possui clientes.");
					}
				}
			} else {
				System.out.println("Nada foi encontrado.");
			}
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
		System.out.println("Fim");
	}

}
